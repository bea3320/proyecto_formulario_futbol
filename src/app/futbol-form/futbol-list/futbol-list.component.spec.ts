import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FutbolListComponent } from './futbol-list.component';

describe('FutbolListComponent', () => {
  let component: FutbolListComponent;
  let fixture: ComponentFixture<FutbolListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FutbolListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FutbolListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
