import { Futbol } from './../models/futbol';
import { Component, Input, OnInit, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-futbol-list',
  templateUrl: './futbol-list.component.html',
  styleUrls: ['./futbol-list.component.scss'],
})
export class FutbolListComponent implements OnInit {
  @Input() public futbolCompleted: Futbol | null = null;
  public futbolList: Array<Futbol | null> = [];
  constructor() {}

  ngOnInit(): void {}

  ngOnChanges(changes: SimpleChanges) {
    this.newFutbol();
  }

  public newFutbol() {
    this.futbolCompleted != undefined || this.futbolCompleted != null
      ? this.futbolList.push(this.futbolCompleted)
      : console.log('Vacio');
  }
}
