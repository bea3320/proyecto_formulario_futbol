import { Futbol } from './models/futbol';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators} from '@angular/forms';

@Component({
  selector: 'app-futbol-form',
  templateUrl: './futbol-form.component.html',
  styleUrls: ['./futbol-form.component.scss']
})
export class FutbolFormComponent implements OnInit {

  public futbolForm: FormGroup;
  public submited: boolean = false;

  public futbolCompleted: Futbol | null = null;

  constructor(private formBuilder: FormBuilder) {
    this.futbolForm = this.formBuilder.group({
      name:['', [Validators.required, Validators.minLength(1)]],
      age:['', [Validators.required, Validators.min(18)]],
      ciudad: ['', [Validators.maxLength(50)]],
      email:['', [Validators.email, Validators.required]],
    });
  }

  ngOnInit(): void {/*empty*/}

public onSubmit():void{
  this.submited=true;

  if(this.futbolForm?.valid){
const FUTBOL: Futbol = {
  name: this.futbolForm.get('name')?.value,
  age: this.futbolForm.get('age')?.value,
  ciudad: this.futbolForm.get('ciudad')?.value,
  email: this.futbolForm.get('email')?.value,
};
this.futbolCompleted = FUTBOL;
this.futbolForm.reset();
this.submited=false;
  }
}
}
