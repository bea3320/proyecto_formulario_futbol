import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FutbolFormComponent } from './futbol-form.component';

describe('FutbolFormComponent', () => {
  let component: FutbolFormComponent;
  let fixture: ComponentFixture<FutbolFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FutbolFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FutbolFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
