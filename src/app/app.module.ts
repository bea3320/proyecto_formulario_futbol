import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FutbolFormComponent } from './futbol-form/futbol-form.component';
import { FutbolListComponent } from './futbol-form/futbol-list/futbol-list.component';

@NgModule({
  declarations: [
    AppComponent,
    FutbolFormComponent,
    FutbolListComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
